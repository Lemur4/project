const FormReview = () => {
    return (
        <div class="form-review characteristics__form-review">
            <form class="form-review__info" action="">
                <h3 class="subtitle form-review__subtitle">
                    Добавить свой отзыв
                </h3>  
                    <div class="form-review__input">
                        <input class="form-review__input-text" type="text" name="name" placeholder="Имя и фамилия" />
                        <input class="form-review__input-number" type="number" name="raiting" min="1" max="5" placeholder="Оценка" />
                    </div>
                    <div class="form-review__textarea">
                        <textarea class="form-review__textarea-box" placeholder="Текст отзыва"></textarea>
                    </div>
                <div class="form-review__button">Отправить отзыв</div>
            </form>
        </div>
    )
}

export default FormReview;
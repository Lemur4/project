const Goods = () => {
    return (
        <div class="goods characteristics__goods">
            <h3 class="subtitle subtitle_margin-bottom">Характеристики товара</h3>
            <div class="menu">
                <ul class="menu__list">
                    <li class="menu__item">Экран: 6.1</li>
                    <li class="menu__item">Встроенная память: 128 ГБ</li>
                    <li class="menu__item">Операционная система: iOS 15</li>
                    <li class="menu__item">Беспроводные интерфейсы: NFC, Bluetooth, Wi-Fi</li>
                    <li class="menu__item">Процессор: <a class="menu__link" href="https://learn.innopolis.university/">Apple A15 Bionic</a></li>
                    <li class="menu__item">Вес: 173 г</li>
                </ul>
            </div>
        </div>
    )
}

export default Goods;
const Table = () => {
    return (
        <div class="table characteristics__table">
            <h3 class="subtitle subtitle_margin-bottom">Сравнение моделей</h3>
            <div class="table__inner">
                <div class="table__column">
                    <h3 class="table__title">Модель</h3>
                    <div class="table__descr">iPhone 11</div>
                    <div class="table__descr">iPhone 12</div>
                    <div class="table__descr">iPhone 13</div>
                </div>
                <div class="table__column">
                    <h3 class="table__title">Вес</h3>
                    <div class="table__descr">194 грамма</div>
                    <div class="table__descr">164 грамма</div>
                    <div class="table__descr">174 грамма</div>
                </div>
                <div class="table__column">
                    <h3 class="table__title">Высота</h3>
                    <div class="table__descr">150.9 мм</div>
                    <div class="table__descr">146.7 мм</div>
                    <div class="table__descr">146.7 мм</div>
                </div>
                <div class="table__column">
                    <h3 class="table__title">Ширина</h3>
                    <div class="table__descr">75.7 мм</div>
                    <div class="table__descr">71.5 мм</div>
                    <div class="table__descr">71.5 мм</div>
                </div>
                <div class="table__column">
                    <h3 class="table__title">Толщина</h3>
                    <div class="table__descr">8.3 мм</div>
                    <div class="table__descr">7.4 мм</div>
                    <div class="table__descr">7.65 мм</div>
                </div>
                <div class="table__column">
                    <h3 class="table__title">Чип</h3>
                    <div class="table__descr">A13 Bionic chip</div>
                    <div class="table__descr">A14 Bionic chip</div>
                    <div class="table__descr">A15 Bionic chip</div>
                </div>
                <div class="table__column">
                    <h3 class="table__title">Объём памяти</h3>
                    <div class="table__descr">до 128 Гб</div>
                    <div class="table__descr">до 256 Гб</div>
                    <div class="table__descr">до 512 Гб</div>
                </div>
                <div class="table__column">
                    <h3 class="table__title">Аккумулятор</h3>
                    <div class="table__descr">до 17 часов</div>
                    <div class="table__descr">до 19 часов</div>
                    <div class="table__descr">до 19 часов</div>
                </div>
            </div>
        </div>
    )
}

export default Table;
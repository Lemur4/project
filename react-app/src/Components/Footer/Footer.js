const Footer = () => {
    return (
        <div class="footer">
            <div class="container footer__container">
                <div class="footer__inner">
                    <div class="footer__contacts">
                        <h5 class="footer__name">
                            © ООО «IphoneShop», 2018-2022
                        </h5>
                        <p class="footer__info">
                            Для уточнения информации звоните по номеру <a href="tel:+79000000000">+7 900 000 0000,</a>
                        </p>
                        <p class="footer__info">а предложения по сотрудничеству отправляйте на почту <a
                            href="mailto:partner@mymarket.com">partner@mymarket.com</a></p>
                    </div>
                    <a class="footer__anchor" href="#logo">Навверх</a>
                </div>
            </div>
        </div>
    )
}

export default Footer;
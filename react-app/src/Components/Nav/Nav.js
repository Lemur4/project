const Nav = ({items}) => {

    return (
        <div class="nav">
            <div class="container">
                <div class="nav__item">
                    {items.map((link) => (
                        <a class="nav__link" 
                            key={link.id} 
                            href="/">
                            {link.name} 
                        </a>
                    ))}
                </div>
            </div>
        </div>
    )
}

export default Nav;